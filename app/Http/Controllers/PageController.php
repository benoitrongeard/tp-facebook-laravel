<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Message;

class PageController extends Controller
{
	public function newsFeed(){
		$user= auth()->user();
		$messages = Message::readableFor($user)->get();
		return view('pages.news-feed',  compact('messages'));
	}
}
