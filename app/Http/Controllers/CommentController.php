<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Message;
use App\Comment;

class CommentController extends Controller
{	

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request, $message_id)
	{

		$message = Message::findOrFail($message_id);

		$message->comments()->create([
			'user_id' => auth()->user()->id,
			'message_id' => $message_id,
			'content' => $request->get('content'),
		]);

		return redirect()->back();
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy($comment_id)
	{
		$comment = Comment::findOrFail($comment_id); 
		$comment->destroy();
		
		return redirect()->back();
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function like(Request $request, $message_id, $comment_id)
	{
		$comment = Comment::findOrFail($comment_id);

		$comment->likes()->create([
			'user_id' => auth()->user()->id,
		]);

		return redirect()->back();
	}
}
