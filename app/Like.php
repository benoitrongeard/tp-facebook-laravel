<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	Public function likable(){
		return $this->morphTo();
	}

	protected $fillable = ['user_id', 'likable_type', 'likable_id'];
}
