<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	public function likes(){
		return $this->morphMany(\App\Like::class, 'likable');
	}

	public function comments(){
		return $this->hasMany(Comment::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function scopeReadableFor($query, $user){
		$friends = $user->friends->pluck('id')->toArray();
		array_push($friends, $user->id);
		return $query->whereIn('user_id', $friends)->orderBy('created_at', 'desc');
	}

	protected $fillable = ['user_id', 'content'];
}
