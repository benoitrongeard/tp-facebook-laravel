<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	public function likes(){
		return $this->morphMany(\App\Like::class, 'likable');
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function message(){
		return $this->belongsTo(Message::class);
	}
	
	protected $fillable = ['message_id', 'user_id', 'content'];
}
