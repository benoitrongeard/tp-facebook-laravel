<div class="commentaire">
	<div class="fb-blue">{{$comment->user->name}}: </div>{{$comment->content}}
	</div>
	<form method="post" action="{{route('messages.comments.likes.store', ['comment' => $comment, 'messages' => $message])}}">
		{{ csrf_field() }}
		{{-- si les likes sont > 0 alors on affiche le nombre de kiff(s) --}}
		@if(count($comment->likes))
			{{count($comment->likes)}}
		@endif
		<button type="submit" class="btn btn-default">Je kiff <i class="glyphicon glyphicon-thumbs-up"></i></button>
	</form>
