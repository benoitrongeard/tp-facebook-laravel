@include('message.form')
{{-- inclure la vue form pour les messages --}}

<h3>Mes messages</h3>

@foreach($messages as $message)
	@include('message.single', [ 'message' => $message ])
@endforeach

{{-- parcourir la liste des messages de l'utilisateur avec @foreach --}}
	{{-- dans la boucle inclure la vue single pour chaque message --}}