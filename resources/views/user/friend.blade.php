<form method="post" action="{{route('users.friends.store', ['users' => $user->id])}}">
	{{ csrf_field() }}
	<div class="ami">
	{{$user->name}}
		@if(!auth()->user()->friends->contains($user))
			<button type="submit" class="btn btn-primary fl-right"> <i class="glyphicon glyphicon-plus"></i></button>
		@endif
	</div>
</form>
		