	<div class="panel panel-default">
		<div class="panel-heading">{{$message->user->name}} - {{$message->created_at->diffForHumans()}}</div>
		<div class="panel-body">

			<div class="message">
				{{-- contenu du message --}}
				{{$message->content}}
            </div>
            <form method="post" action="{{route('messages.likes.store', ['messages' => $message])}}">
                {{ csrf_field() }}
				{{-- si les likes sont > 0 alors on affiche le nombre de kiff(s) --}}
				@if(count($message->likes))
					{{count($message->likes)}}
				@endif
                <button type="submit" class="btn btn-default right">Je kiff <i class="glyphicon glyphicon-thumbs-up"></i></button>
            </form>


            @foreach($message->comments as $comment)
			<div class="padding commentaires">
				{{-- inclure la vue single du commentaire $comment --}}
				@include('comment.single', [ 'comment' => $comment, 'message' => $message])
			</div>
            @endforeach
			{{-- inclure la vue form du message $message --}}
			@include('comment.form', [ 'message' => $message])
		</div>
	</div>
