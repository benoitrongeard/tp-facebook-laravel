@extends('layout.facebook')

@section('content')
    <h2>{{$utilisateurCourant->name}}</h2>
    {{-- inclure la vue single de l'utilisateur $user --}}
    @include('user.single', [ 'utilisateurCourant' => $utilisateurCourant, 'messages' => $messages])
@endsection